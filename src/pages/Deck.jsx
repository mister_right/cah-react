import { Icon } from '@iconify/react';
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import instance from "../services/Common";

import { useDispatch, useSelector } from 'react-redux'
import { addNotification } from '../store/common'

export function Deck() {
    const dispatch = useDispatch()
    const loggedIn = useSelector(state => state.common.loggedIn)
    const username = useSelector(state => state.common.username)

    const { id } = useParams()
    const [data, setData] = useState()
    const [deckCards, setDeckCards] = useState()

    const [cardIndex, setCardIndex] = useState()
    const [card, setCard] = useState({})

    const [page, setPage] = useState(1)
    const [pageInput, setPageInput] = useState(1)
    const [pageSize, setPageSize] = useState(60)
    const [deckSize, setDeckSize] = useState()

    const [loading, setLoading] = useState(false)

    const getDeckData = () => {
        instance.get(`/decks/${id}`).then(res => {
            console.log(res.data)
            setData(res.data)
        })
    }

    const getDeckCards = () => {
        instance.get(`/decks/${id}/cards`, { params: { page, size: pageSize } }).then(res => {
            setDeckSize(parseInt(res.data.count))
            const cards = res.data.cards
            for (let i of cards) {
                i['color'] = i.black ? 'black' : 'white'
            }
            setDeckCards(cards)
        })
    }

    const setPublic = async () => {
        instance.patch(`/decks/${id}`, { public: !data.deck.public }).then(res => {
            data.deck.public = res.data.public
            setData(dd => ({ ...dd, ...data }))
        })
    }

    const fileChange = (e) => {
        if (e.target.files[0]) {
            let formData = new FormData();
            formData.append("file", e.target.files[0]);
            instance.post('/cards/file-import', formData, { params: { deck_id: id } }).then(() => {
                dispatch(addNotification({ text: 'Cards are importing.', type: 0 }))
            })
        }
    }

    const isCardValid = () => {
        if ('text' in card === false || card.text.length === 0) {
            dispatch(addNotification({ text: 'Invalid card text.', type: 2 }))
            return false
        }
        if ('color' in card === false || ['black', 'white'].includes(card.color) === false) {
            dispatch(addNotification({ text: 'Select card color.', type: 2 }))
            return false
        }
        if (card.color === 'black' && 'fields' in card === false) {
            dispatch(addNotification({ text: 'Give number of fields.', type: 2 }))
            return false
        }
        if (card.color === 'black' && (card.fields <= 0 || card.fields > 4)) {
            dispatch(addNotification({ text: 'Invalid number of fields.', type: 2 }))
            return false
        }
        return true
    }

    const saveAsNewCard = () => {
        if (isCardValid() === false) return
        instance.post('/cards', { deck_id: parseInt(id), text: card.text, color: card.color, fields: card.fields || null }).then(res => {
            dispatch(addNotification({ text: `Card "${res.data.text}" added to deck.`, type: 0 }))
            setCardIndex(null)
            if (page === 1) getDeckCards()
            else setPage(1)
        })
    }

    const overwrite = () => {
        if (isCardValid() === false) return
        instance.put(`/cards/${card.card_id}`, { deck_id: parseInt(id), text: card.text, color: card.color, fields: card.fields || null }).then(res => {
            setDeckCards([...deckCards.slice(0, cardIndex), card, ...deckCards.slice(cardIndex + 1, deckCards.length)])
            dispatch(addNotification({ text: `Card ${res.data.card_id} changed.`, type: 0 }))
            setCardIndex(null)
        })
    }

    const removeCard = () => {
        instance.delete(`/cards/${deckCards[cardIndex].card_id}`).then(res => {
            dispatch(addNotification({ text: `Card ${res.data.card_id} removed.`, type: 0 }))
            getDeckCards()
        })
    }

    const handleCard = (e) => {
        card[e.target.id] = e.target.value
        setCard({ ...card })
    }

    const downloadExample = () => {
        instance.get('/cards/example', { responseType: 'blob' }).then(res => {
            const href = URL.createObjectURL(res.data)
            const link = document.createElement('a')
            link.href = href
            link.setAttribute('download', 'example.csv')
            document.body.appendChild(link)
            link.click()
            document.body.removeChild(link)
            URL.revokeObjectURL(href)
        })
    }

    const decrementPage = () => {
        if (page === 1) return
        setPage(page - 1)
    }

    const incrementPage = () => {
        if (page === Math.ceil(deckSize / pageSize)) return
        setPage(page + 1)
    }

    const handlePage = (e) => {
        e.preventDefault()
        if (!pageInput) {
            setPageInput(page)
            return
        }
        let n = parseInt(pageInput)
        console.log(n)
        const max = Math.ceil(deckSize / pageSize)
        if (n < 1) n = 1
        else if (n > max) n = max
        if (n === page) {
            setPageInput(n)
            return
        }
        setPage(n)
    }

    useEffect(() => {
        if (cardIndex === null || cardIndex === undefined || cardIndex === -1) {
            setCard({})
            return
        }
        const tmp = deckCards[cardIndex]
        setCard({ text: tmp.text, color: tmp.color, fields: tmp.fields, card_id: tmp.card_id })
    }, [cardIndex])

    useEffect(() => {
        getDeckCards()
        setPageInput(page)
    }, [page])

    useEffect(() => {
        getDeckData()
    }, [])

    return (
        <>
            {data ?
                <div className="container mx-auto my-4 h-full text-slate-900 flex flex-col select-none">
                    {cardIndex != undefined && cardIndex != null ?
                        <>
                            <div onClick={() => setCardIndex(null)} className="absolute w-full h-full top-0 left-0 bg-slate-400 opacity-50"></div>
                            <div className="absolute bg-slate-600 top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 rounded space-x-4 flex items-center p-12">
                                <div className='flex flex-col space-y-2'>
                                    <input id="text" placeholder='card text' className="input-primary w-96 h-12" type="text" value={card.text || ''} onChange={handleCard} />
                                    <select id="color" className="input-primary h-12 px-4" value={card.color || ""} onChange={handleCard}>
                                        <option value="">color</option>
                                        <option value="black">black</option>
                                        <option value="white">white</option>
                                    </select>
                                    {card.color === 'black' ? <input id="fields" placeholder='number of fields' className="input-primary h-12" type="number" value={card.fields || ''} onChange={handleCard} /> : null}
                                    {cardIndex === -1 ? null : <button onClick={overwrite} className="btn-primary h-12">overwrite</button>}
                                    <button onClick={saveAsNewCard} className="btn-primary h-12">save as new card</button>
                                    {cardIndex === -1 ? null : <button onClick={removeCard} className="btn-primary h-12 bg-rose-500 hover:bg-rose-700">remove card</button>}
                                </div>
                            </div>
                        </>
                        : null}
                    <div className="flex w-full">
                        <div className="w-full flex flex-col md:flex-row items-center justify-between">
                            <h1>Deck {data.deck.name}</h1>
                            {loggedIn === true && data.deck.owner === username ?
                                <div className="flex space-x-2 items-center">
                                    <button onClick={setPublic} className="btn-primary text-xs p-2">{data.deck.public ? 'set private' : 'set public'}</button>
                                    <button onClick={() => setCardIndex(-1)} className="btn-primary text-xs p-2">add card</button>
                                    <button onClick={downloadExample} className="btn-primary text-xs p-2">download example</button>
                                    <label>
                                        <p className="btn-primary text-xs p-2">import cards from file</p>
                                        <input className="hidden" onChange={fileChange} type="file" />
                                    </label>
                                </div>
                                : null}
                        </div>
                    </div>
                    <div className="h-full rounded-xl overflow-y-auto overflow-x-hiddenmt mt-4">
                        <div className='flex w-full h-12 items-center justify-center space-x-2'>
                            <button onClick={decrementPage} disabled={page === 1 ? true : false}>
                                <Icon className='text-2xl' icon="material-symbols:chevron-left" />
                            </button>
                            <form onSubmit={handlePage}>
                                <input type="number" className='input-primary w-20 border-2 border-slate-600' value={pageInput || ''} onBlur={handlePage} onChange={e => setPageInput(e.target.value)} />
                            </form>
                            <p>from {Math.ceil(deckSize / 50)}</p>
                            <button onClick={incrementPage} disabled={Math.ceil(deckSize / pageSize) === page ? true : false}>
                                <Icon className='text-2xl' icon="material-symbols:chevron-right" />
                            </button>
                        </div>
                        {!deckCards || deckCards.length === 0 ? <p>no cards in deck</p> :
                            <ul className='flex flex-wrap'>
                                {deckCards.map((card, index) => (
                                    
                                    <li id={card.card_id} key={`c-${index}`} className={`m-1 card ${card.color === 'black' ? 'black' : 'white'}`} onClick={() => { loggedIn === true && data.deck.owner === username ? setCardIndex(index) : null }}>{card.text}</li>
                                ))}
                            </ul>
                        }
                    </div>
                </div>
                : null
            }
        </>
    )
}