import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import { Link } from "react-router-dom"
import instance from "../services/Common"

import { useSelector, useDispatch } from 'react-redux'
import { logIn } from '../store/common'

export const Login = () => {
    const dispatch = useDispatch()
    const loggedIn = useSelector((state) => state.common.loggedIn)

    const navigate = useNavigate()

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')

    const handleLogIn = (e) => {
        e.preventDefault()
        instance.post('/users/login', { username, password }).then(res => {
            console.log(res.data)
            dispatch(logIn(res.data))
            navigate('/')
        })
    }

    useEffect(() => {
        if (loggedIn) navigate('/')
    })

    return (
        <>
            <div className="w-full h-screen bg-slate-300 flex justify-center items-center">
                <div>
                    <form onSubmit={handleLogIn} className="flex flex-col space-y-4">
                        <input placeholder="login" onChange={e => setUsername(e.target.value)} type="text" className="input-primary" />
                        <input placeholder="password" onChange={e => setPassword(e.target.value)} type="password" className="input-primary" />
                        <button className="btn-primary">log in</button>
                    </form>
                    <div className="flex w-full justify-between">
                        <Link className="text-slate-800 underline" to='/'>main page</Link>
                        <Link className="text-slate-800 underline" to='/register'>create account</Link>
                    </div>
                </div>
            </div>
        </>
    )
}