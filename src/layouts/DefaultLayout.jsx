import { Icon } from '@iconify/react';
import { Link, Outlet } from "react-router-dom";

import { useSelector, useDispatch } from 'react-redux'
import { logOut } from '../store/common'

export const DefaultLayout = () => {
    const dispatch = useDispatch()
    const loggedIn = useSelector((state) => state.common.loggedIn)
    const username = useSelector((state) => state.common.username)


    const handleLogOut = () => {
        dispatch(logOut())
    }

    return (
        <>
            <main className="absolute top-0 left-0 w-full bg-slate-300 h-full pb-8 pt-12 px-1">
                <Outlet />
            </main>
            <header className="w-full h-12 bg-slate-400 absolute">
                <div className='container mx-auto w-full h-full flex items-center justify-between'>
                    <Link to='/'>
                        <Icon className='text-4xl text-slate-900' icon="mdi:cards" />
                    </Link>
                    {loggedIn ?
                        <div className="flex space-x-4 items-center px-1">
                            <Link className='underline text-slate-900' to='/profile'>{username}</Link>
                            <button className='bg-inherit underline text-slate-900' onClick={handleLogOut}>log out</button>
                        </div>
                        :
                        <Link className='underline text-slate-900' to='/login'>login</Link>
                    }
                </div>
            </header>
        </>
    )
}